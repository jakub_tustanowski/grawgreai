package pl.edu.agh.to2.grawgre.ai.tests;

import pl.edu.agh.to2.grawgre.ai.BotAI;
import pl.edu.agh.to2.grawgre.ai.PokerHand;
import pl.edu.agh.to2.grawgre.ai.PokerWinningStrategy;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jakubtustanowski on 16/01/2017.
 */
public class MockPWS implements PokerWinningStrategy{

    public MockPWS() {
    }

    @Override
    public List<Player> checkWinner(GameState gameState) {
        List<Integer> dice = Arrays.asList(4,4,4,3,1);
        Player player = new Player("jan1",0, dice);
        return Collections.singletonList(player);
    }

    @Override
    public PokerHand calculateHand(List<Integer> dice) {
        return new MockHand(dice.toArray(new Integer[0]));
    }
}
