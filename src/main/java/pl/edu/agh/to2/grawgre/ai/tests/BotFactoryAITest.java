package pl.edu.agh.to2.grawgre.ai.tests;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.agh.to2.grawgre.ai.BotAI;
import pl.edu.agh.to2.grawgre.ai.BotFactoryAI;
import pl.edu.agh.to2.grawgre.ai.PokerEasyStrategy;
import pl.edu.agh.to2.grawgre.ai.PokerHardStrategy;
import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.model.BotConfiguration;
import pl.edu.agh.to2.grawgre.model.BotLevel;
import pl.edu.agh.to2.grawgre.model.Configuration;
import pl.edu.agh.to2.grawgre.model.GameType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by jakubtustanowski on 16/01/2017.
 */
public class BotFactoryAITest {


    @Test
    public void constructor() throws Exception
    {
        BotFactoryAI botFactoryAI = new BotFactoryAI();
        Assert.assertEquals(0, botFactoryAI.getNumber());
    }

    @Test
    public void makeBots() throws Exception {
        BotFactoryAI botFactoryAI = new BotFactoryAI();

        List<BotConfiguration> botConfigurationList = new ArrayList<>();

        botConfigurationList.add(new BotConfiguration("Jan", BotLevel.EASY));
        botConfigurationList.add(new BotConfiguration("Paweł", BotLevel.MASTER));

        Configuration configuration = new Configuration(GameType.POKER, 5, botConfigurationList, 0);

        List<Bot> botList = botFactoryAI.makeBots(configuration);

        BotAI botJan = (BotAI)botList.get(0);
        BotAI botPaweł = (BotAI)botList.get(1);

        Assert.assertEquals("Jan1",botJan.getNick());
        Assert.assertEquals("Paweł2",botPaweł.getNick());

        Assert.assertTrue(botJan.getStrategy() instanceof PokerEasyStrategy);
        Assert.assertTrue(botPaweł.getStrategy() instanceof PokerHardStrategy);
    }
}