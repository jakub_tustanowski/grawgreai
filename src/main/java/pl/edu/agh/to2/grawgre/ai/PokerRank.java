package pl.edu.agh.to2.grawgre.ai;

/**
 * Created by jakubtustanowski on 16/01/2017.
 */
public interface PokerRank extends Comparable<PokerRank> {
    public int getRank();
}
