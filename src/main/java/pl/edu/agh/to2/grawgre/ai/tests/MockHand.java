package pl.edu.agh.to2.grawgre.ai.tests;

import pl.edu.agh.to2.grawgre.ai.PokerHand;
import pl.edu.agh.to2.grawgre.ai.PokerRank;

/**
 * Created by jakubtustanowski on 16/01/2017.
 */
public class MockHand implements PokerHand{

    int repeats = 0;
    int number = 0;
    int[] dice;

    public MockHand(Integer[] dice) {
        int current;
        for (int i = 1; i <= 6; i++) {
            current = 0;
            for (int j = 0; j < 5; j++) {
                if (dice[j] == i)
                    current++;
            }
            if (current > repeats) {
                repeats = current;
                number = i;
            }
        }
    }

    @Override
    public int compareTo(PokerHand otherPokerHand) {
        if(this.repeats>((MockHand) otherPokerHand).repeats)
            return 1;
        if(this.repeats<((MockHand) otherPokerHand).repeats)
            return -1;
        if(this.number>((MockHand) otherPokerHand).number)
            return 1;
        if(this.number<((MockHand) otherPokerHand).number)
            return -1;
        return 0;
    }

    @Override
    public PokerRank getRank() {
        return new PokerRank() {
            @Override
            public int getRank() {
                return 5;
            }

            @Override
            public int compareTo(PokerRank o) {
                return 0;
            }
        };
    }
}
