package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import java.math.BigInteger;
import java.util.*;

import static java.lang.Math.pow;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public abstract class PokerStrategy extends Strategy {

    protected PokerWinningStrategy pws;

    public PokerStrategy(String nick) {
        super(nick);
    }

    public Move makeMove(GameState gameState)
    {
        Player self = getPlayer(gameState.getListOfPlayers());
        List<Integer> selfDice = self.getDice();
        List<Integer> bestDice;
        bestDice = pws.checkWinner(gameState).get(0).getDice();
        Double[] goodRerolls = new Double[32];
        Double[] allRerolls  = new Double[32];
        Arrays.fill(goodRerolls, 0.0);
        Arrays.fill(allRerolls, 0.0);
        PokerHand bestHand = pws.calculateHand(bestDice);
        checkAndGoDeeper(0,selfDice, bestHand, goodRerolls, allRerolls, 0);

        int finalHash = findMaxHash(goodRerolls,allRerolls);

        return hashToMove(finalHash);
    }

    private void checkAndGoDeeper(int currentDice, List<Integer> tempDice, PokerHand bestHand, Double[] goodRerolls, Double[] allRerolls, int rerollHash)
    {
        int myRerollHash;
        PokerHand tempHand;

        for(int i=0;i<6;i++)
        {
            tempDice.set(currentDice, nextDieValue(tempDice.get(currentDice)));
            if(i==5)
            {
                myRerollHash = setRerollHash(rerollHash, currentDice, false);
            }
            else
            {
                myRerollHash = setRerollHash(rerollHash, currentDice, true);
            }

            allRerolls[myRerollHash]++;
            tempHand = pws.calculateHand(tempDice);
            if (bestHand.compareTo(tempHand)<0) {
                goodRerolls[myRerollHash]+=assertHandValue(tempHand, bestHand);
            }
            if(currentDice<4)
                checkAndGoDeeper(currentDice+1, tempDice, bestHand, goodRerolls, allRerolls, myRerollHash);
        }
    }

    protected abstract Double assertHandValue(PokerHand tempHand, PokerHand currentBestHand);

    public void setPws(PokerWinningStrategy pws) {
        this.pws = pws;
    }
}
