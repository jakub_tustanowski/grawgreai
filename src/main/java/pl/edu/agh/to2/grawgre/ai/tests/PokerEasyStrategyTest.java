package pl.edu.agh.to2.grawgre.ai.tests;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.agh.to2.grawgre.ai.PokerEasyStrategy;
import pl.edu.agh.to2.grawgre.ai.tests.MockPWS;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.GameStatus;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;


import java.lang.reflect.Array;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by jakubtustanowski on 16/01/2017.
 */
public class PokerEasyStrategyTest {
    @Test
    public void makeMove() throws Exception {
        Assert.assertEquals(new HashSet(Arrays.asList(3,4)), prepareStrategy().makeMove(prepareGameState(Arrays.asList(3,3,3,1,2))).getDice());
        Assert.assertEquals(new HashSet(Arrays.asList(0,3,4)), prepareStrategy().makeMove(prepareGameState(Arrays.asList(3,5,5,1,2))).getDice());
    }

    private GameState prepareGameState(List<Integer> list)
    {
        Player player = new Player("jan1",0, list);
        return new GameState(Collections.singletonList(player), GameStatus.STARTED, "jan1");
    }
    private PokerEasyStrategy prepareStrategy()
    {
        PokerEasyStrategy pokerEasyStrategy = new PokerEasyStrategy("jan1");
        pokerEasyStrategy.setPws(new MockPWS());
        return pokerEasyStrategy;
    }
}