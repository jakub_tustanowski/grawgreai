package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Player;

import java.util.List;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public interface PokerWinningStrategy {
    public List<Player> checkWinner(GameState gameState);
    public PokerHand calculateHand( List<Integer> dice);
}
