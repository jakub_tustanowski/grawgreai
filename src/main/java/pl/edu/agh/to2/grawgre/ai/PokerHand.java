package pl.edu.agh.to2.grawgre.ai;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public interface PokerHand {

    public int compareTo(PokerHand otherPokerHand);

    public PokerRank getRank();
}
